import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import loan from '../models/loan';

@Injectable({
  providedIn: 'root'
})
export class LoanService {

  private dbPath = '/loan';

  tutorialsRef: AngularFireList<loan> = null;

  constructor(private db: AngularFireDatabase) {
    this.tutorialsRef = db.list(this.dbPath);
  }

  getAll(): AngularFireList<loan> {
    return this.tutorialsRef;
  }

  create(loan: loan): any {
    return this.tutorialsRef.push(loan);
  }

  update(key: string, value: any): Promise<void> {
    return this.tutorialsRef.update(key, value);
  }

  delete(key: string): Promise<void> {
    return this.tutorialsRef.remove(key);
  }

  deleteAll(): Promise<void> {
    return this.tutorialsRef.remove();
  }
}