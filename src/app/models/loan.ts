export default class Loan {
    key: string;
    name: string;
    phone: string;
    amount:number;
    address:string;
    parts:number;
    url:string;
    published = false;
  }

 