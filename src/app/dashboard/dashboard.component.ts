import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import Loan from '../models/loan';
import {LoanService} from '../service/loan.service'
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})


export class DashboardComponent implements OnInit {
  newForm: FormGroup;
List = ["1","2"];
amount:number;
parts:number;
total:number;


loan: Loan = new Loan();
submitted = false;
  imagePath: any;
  url: string | ArrayBuffer;

  constructor(private fb: FormBuilder ,public dialogRef: MatDialogRef<DashboardComponent>,
    @Inject(MAT_DIALOG_DATA) public message: string,private loanService: LoanService) {

      this.newForm= new FormGroup({

        name: new FormControl("",Validators.required),
        phone: new FormControl("",Validators.required),
        address: new FormControl("",Validators.required),
        amount: new FormControl("",Validators.required),
        parts: new FormControl("",Validators.required),
        value: new FormControl(""),
      });
     }

  ngOnInit(): void {

  }

  onFileChanged(event) {
    const files = event.target.files;
    if (files.length === 0)
        return;

    const mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
        this.message = "Only images are supported.";
        return;
    }

    const reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => { 
        this.url = reader.result; 
    }
}
  saveLoan(): void {
    this.loanService.create(this.loan).then(() => {
      console.log('Created new item successfully!');
      this.submitted = true;
    });
  }

  newLoan(): void {
    this.submitted = false;
    this.loan = new Loan();
  }
  checkout(){
    this.amount = this.newForm.controls['amount'].value;
    this.parts = this.newForm.controls['parts'].value;
   
    this.total=this.amount/this.parts;
    this.newForm.controls.value.setValue(this.total);
    console.log(this.amount,this.parts,this.total);
  }
  
  closeComment() {
    this.dialogRef.close();
}
}
