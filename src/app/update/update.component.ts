import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LoanService } from '../service/loan.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent implements OnInit {
  newForm: any;
loan:any;
  message: string;
  constructor(private fb: FormBuilder ,public dialogRef: MatDialogRef<UpdateComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,private loanService: LoanService) {
      this.newForm= fb.group({

        name: new FormControl("",Validators.required),
        phone: new FormControl("",Validators.required),
        address: new FormControl("",Validators.required),
        amount: new FormControl("",Validators.required),
        parts: new FormControl("",Validators.required),
        value: new FormControl("").disable(),
      });
     }

  ngOnInit(): void {
    this.loan = this.data;
    console.log(this.data)
  }

  updateLoan(): void {
    const data = {
      name: this.loan.name,
     phone: this.loan.phone
    };

    this.loanService.update(this.loan.key, data)
      .then(() => this.message = 'The tutorial was updated successfully!')
      .catch(err => console.log(err));
  }


  deleteloan(): void {
    this.loanService.delete(this.loan.key)
      .then(() => {
        // this.refreshList.emit();
        this.message = 'The tutorial was updated successfully!';
      })
      .catch(err => console.log(err));
  }
 
  closeComment() {
    this.dialogRef.close();
}
}
