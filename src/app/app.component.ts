import { Component } from '@angular/core';
import { AuthenticationService } from './service/authentication.service';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { FormBuilder, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MatDialog } from '@angular/material/dialog';
import { LoanService } from './service/loan.service';
import { map } from 'rxjs/operators';
import { UpdateComponent } from './update/update.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {



  loans: any;
  currentloan = null;
  currentIndex = -1;
  name = '';

  title = 'firebaseLogin';
dashboard = false;
  selectedVal: string;
  register = false;
  responseMessage: string = '';
  responseMessageType: string = '';
  emailInput: string;
  passwordInput: string;
  isForgotPassword: boolean;
  userDetails: any;
  displayLoginDetails: boolean = true;
  // name = 'Angular 6';
  // url = 'https://docs.google.com/forms/d/e/1FAIpQLSdqBLC3ZJ8sNRb0xx2Byp7VVFaaz53f3zwGFaKS0p3leDpfsw/formResponse';

  // interestOptions = [
  //   { id: 1, label: '看AMOS直播', value: 'AMOS', selected: false },
  //   { id: 2, label: '看AMOS推坑', value: 'PUSH MAN AMOS', selected: false },
  //   { id: 3, label: '看AMOS炙燒牛排', value: 'FIRE MAN AMOS', selected: false },
  //   { id: 4, label: '看ALEX直播', value: 'ALEX', selected: false },
  //   { id: 5, label: '看TOMMY直播', value: 'TOMMY', selected: false },
  //   { id: 6, label: '看甲級工具人直播', value: 'franmo', selected: false },
  // ];

  // fieldMapping = {
  //   name: 'entry.939695647',
  //   gender: 'entry.1798110377',
  //   email: 'entry.1117032030',
  //   interest: 'entry.342875127',
  //   birthday: 'entry.1091494304',
  //   birthdayTime: 'entry.220326002',
  //   jump: 'entry.1922085348',
  //   memo: 'entry.2141791396'
  // };

  // formData = this.fb.group({
  //   name: ['', Validators.required],
  //   gender: ['', Validators.required],
  //   email: ['', [Validators.required, Validators.email]],
  //   interest: this.fb.array(this.buildinterest(), this.interestRequired()),
  //   birthday: '',
  //   birthdayTime: '',
  //   jump: ['', Validators.required],
  //   memo: ''
  // });

  // get interest() {
  //   return this.formData.get('interest') as FormArray;
  // }

  // interestRequired() {
  //   return control => control.value.some((item) => item) ? null : { 'interest rquired': { valid: false } }
  // }

  // buildinterest() {
  //   return this.interestOptions.map(skill => {
  //     return this.fb.control(skill.selected);
  //   });
  // }

  // save() {
  //   if (this.formData.valid) {
  //     const rawValue = this.formData.getRawValue();      
  //     let body = new HttpParams();
  //     Object.entries(rawValue).forEach(([key, value]) => {
  //       if (key === 'interest') {
  //         value.forEach((v, idx) => {
  //           if (v) {
  //             body = body.append(this.fieldMapping[key], this.interestOptions[idx].label);
  //           }
  //         })
  //       } else {
  //         body = body.append(this.fieldMapping[key], value);
  //       }
  //     })
  //     const httpOptions = {
  //       headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
  //     };
  //     this.http.post(this.url, body, httpOptions).subscribe(() => { }, (err) => { });
  //   } else {
  //     console.log('form is invalid');
  //   }
  // }

  constructor(
    private authService: AuthenticationService,private fb: FormBuilder, private http: HttpClient,private router: Router,public dialog: MatDialog,private loanService: LoanService
  ) {
    this.selectedVal = 'login';
    this.isForgotPassword = false;

  }
  ngOnInit(): void {
    this.retrieveLoans();
  }

  refreshList(): void {
    this.currentloan = null;
    this.currentIndex = -1;
    this.retrieveLoans();
  }

  retrieveLoans(): void {
    this.loanService.getAll().snapshotChanges().pipe(
      map(changes =>
        changes.map(c =>
          ({ key: c.payload.key, ...c.payload.val() })
        )
      )
    ).subscribe(data => {
      this.loans = data;
  
      console.log(this.loans);


    });
  }

  setActiveLoan(loan, index): void {
    this.currentloan = loan;
    this.currentIndex = index;
  }

  removeAllLoans(): void {
    this.loanService.deleteAll()
      .then(() => this.refreshList())
      .catch(err => console.log(err));
  }
  // Comman Method to Show Message and Hide after 2 seconds
  showMessage(type, msg) {
    this.responseMessageType = type;
    this.responseMessage = msg;
    setTimeout(() => {
      this.responseMessage = "";
    }, 2000);
  }

  // Called on switching Login/ Register tabs
  public onValChange(val: string) {
    this.showMessage("", "");
    this.selectedVal = val;
  }

  // Check localStorage is having User Data
  isUserLoggedIn() {
    this.userDetails = this.authService.isUserLoggedIn();
  }

  // SignOut Firebase Session and Clean LocalStorage
  logoutUser() {
    this.dashboard = false;
    this.authService.logout()
      .then(res => {
        console.log(res);
        this.userDetails = undefined;
        localStorage.removeItem('user');
      }, err => {
        this.showMessage("danger", err.message);
      });
  }

  // Login user with  provided Email/ Password
  loginUser() {
    console.log("login");
    this.responseMessage = "";
    this.authService.login(this.emailInput, this.passwordInput)
      .then(res => {
        console.log(res);
        this.showMessage("success", "Successfully Logged In!");
        this.displayLoginDetails = false;
        this.isUserLoggedIn();
        this.gotodashboard();
      }, err => {
        this.showMessage("danger", err.message);
      });
  }
  gotodashboard() {
    console.log("*ngIf = displayLoginDetails");
    this.dashboard = true;
  }

  // Register user with  provided Email/ Password
  registerUser() {
    this.authService.register(this.emailInput, this.passwordInput)
      .then(res => {

        // Send Varification link in email
        this.authService.sendEmailVerification().then(res => {
          console.log(res);
          this.isForgotPassword = false;
          this.showMessage("success", "Registration Successful! Please Verify Your Email");
        }, err => {
          this.showMessage("danger", err.message);
        });
        this.isUserLoggedIn();


      }, err => {
        this.showMessage("danger", err.message);
      });
  }

  // Send link on given email to reset password
  forgotPassword() {
    this.authService.sendPasswordResetEmail(this.emailInput)
      .then(res => {
        console.log(res);
        this.isForgotPassword = false;
        this.showMessage("success", "Please Check Your Email");
      }, err => {
        this.showMessage("danger", err.message);
      });
  }

  // Open Popup to Login with Google Account
  googleLogin() {
    this.authService.loginWithGoogle()
      .then(res => {
        console.log(res);
        this.showMessage("success", "Successfully Logged In with Google");
        this.isUserLoggedIn();
      }, err => {
        this.showMessage("danger", err.message);
      });
  }

  goregister(){
    this.register = true;
  }
  gologin(){
    this.register=false;
  }

  openDialog(): void {
    this.dialog.open(DashboardComponent, {
      width: '80%',
      height:'80%',
      // data: 'failure',
  //    panelClass: 'custom-dialog-container'
    });
  }

  update(loan):void{
    console.log(loan)
      this.dialog.open(UpdateComponent, {
        width: '80%',

        data: loan,
    //    panelClass: 'custom-dialog-container'
      });
    

  }
}
