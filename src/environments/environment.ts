// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig  :{
    apiKey: "AIzaSyBJZtkceo-QfY94GYwZFArUZkVU7l2gak8",
    authDomain: "feemanagement-53a6f.firebaseapp.com",
    databaseURL: "https://feemanagement-53a6f.firebaseio.com",
    projectId: "feemanagement-53a6f",
    storageBucket: "feemanagement-53a6f.appspot.com",
    messagingSenderId: "324545042224",
    appId: "1:324545042224:web:b22bbdd7a4f7786f58daf5",
    measurementId: "G-NXPZWSPX0N"
  }
};



/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
